const path = require("path");
const PurgecssPlugin = require("purgecss-webpack-plugin");
const glob = require("glob-all");

module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        "@img": path.resolve(__dirname, "./src/assets/img"),
        "@styles": path.resolve(__dirname, "./src/assets/styles"),
        "@components": path.resolve(__dirname, "./src/components"),
        "@views": path.resolve(__dirname, "./src/views"),
        "@mixins": path.resolve(__dirname, "./src/helpers/mixins"),
        "@filters": path.resolve(__dirname, "./src/helpers/filters"),
        "@utils": path.resolve(__dirname, "./src/utils.js")
      }
    }

    // plugins: [
    //   new PurgecssPlugin({
    //     paths: glob.sync([
    //       path.join(__dirname, "./public/**/*.html"),
    //       path.join(__dirname, "./src/**/*.vue"),
    //       path.join(__dirname, "./src/**/*.js")
    //     ])
    //   })
    // ]
  },

  devServer: {
    port: "1881",
    https: false
  }
};
