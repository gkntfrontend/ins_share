module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    "plugin:vue/strongly-recommended"
  ],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
		"quotes": [1, "double", "avoid-escape"],
		"comma-dangle": [
			1, {
				"arrays": "never",
				"objects": "never",
				"imports": "never",
				"exports": "never",
				"functions": "never"
			}
		],
		semi: [
			1,
			"always",
			{
				"omitLastInOneLineBlock": true
			}
		],
    "vue/html-indent": [
      2,
      "space",
      {
        attribute: 1,
        baseIndent: 1,
        closeBracket: 0,
        alignAttributesVertically: false,
        ignores: []
      }
    ]
  },
  parserOptions: {
    parser: "babel-eslint"
  }
};
