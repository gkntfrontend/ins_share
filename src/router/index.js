import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("@views/static/Home.vue")
  },
  {
    path: "/about",
    name: "about",
    component: () => import("@views/static/About.vue")
  },
  {
    path: "/auth",
    component: () => import("@views/auth/Layout.vue"),
    redirect: { name: "Login" },
    children: [
      {
        path: "register",
        name: "Register",
        component: () => import("@views/auth/pages/Register.vue")
      },
      {
        path: "login",
        name: "Login",
        component: () => import("@views/auth/pages/Login.vue")
      },
      {
        path: "forgot-password",
        name: "ForgotPassword",
        component: () => import("@views/auth/pages/ForgotPassword.vue")
      }
    ]
  },
  {
    path: "/client",
    component: () => import("@views/client/Layout.vue"),
    redirect: { name: "ProfessionForm" },
    children: [
      {
        path: "profession-form",
        name: "ProfessionForm",
        component: () => import("@views/client/pages/ProfessionForm"),
        redirect: { name: "ProfessionFormStepOne" },
        children: [
          {
            path: "1",
            name: "ProfessionFormStepOne",
            component: () =>
              import("@views/client/pages/ProfessionForm/StepOne")
          },
          {
            path: "2",
            name: "ProfessionFormStepTwo",
            component: () =>
              import("@views/client/pages/ProfessionForm/StepTwo")
          },
          {
            path: "3",
            name: "ProfessionFormStepThree",
            component: () =>
              import("@views/client/pages/ProfessionForm/StepThree")
          }
        ]
      },
      {
        path: "user",
        name: "User",
        component: () => import("@views/client/pages/User/UserLayout")
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
