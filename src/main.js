import Vue from "vue";
import BootstrapVue from "bootstrap-vue";
import "./plugins/vue-meta";
import "./plugins/vue-select";
import "./plugins/vee-validate";
import "./plugins/fontawesome";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./assets/styles/scss/main.scss";

Vue.use(BootstrapVue);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
