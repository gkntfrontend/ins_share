import Vue from "vue";

export default {
  namespaced: true,
  state: {
    profession: "",
    companyName: "",
    companyAddress: "",
    companyPhone: null,
    companyWebsite: "",
    currentCity: "",
    servingRegions: [],
    subscription: null,
    requests: null
  },

  mutations: {
    SET_PROFESSION: (state, payload) => {
      Vue.set(state, "profession", payload);
    },
    SET_COMPANY_DATA: (state, payload) => {
      let { company_name, company_address, company_phone, company_website } = payload;

      Vue.set(state, "companyName", company_name);
      Vue.set(state, "companyAddress", company_address);
      Vue.set(state, "companyPhone", company_phone);
      Vue.set(state, "companyWebsite", company_website);
    },
    SET_WORK_DATA: (state, payload) => {
      let { current_city, serving_regions, x, y } = payload;

      state.currentCity = current_city;
      state.servingRegions = serving_regions;
      state.subscription = x;
      state.requests = y;
    }
  },

  actions: {
    setProfession({ commit }, payload) {
      commit("SET_PROFESSION", payload);
    },
    setCompanyData({ commit }, payload) {
      commit("SET_COMPANY_DATA", payload);
    },
    setWorkData({ commit }, payload) {
      commit("SET_WORK_DATA", payload);
    }
  }
};
