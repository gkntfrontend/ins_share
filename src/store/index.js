import Vue from "vue";
import Vuex from "vuex";
import professionForm from "./modules/profession_form";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    professionForm
  }
});

export default store;
