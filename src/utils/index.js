// ! UUID Timestamp Generator
// ? npm i uuid -D
import uuidv1 from "uuid/v1";

// ! SVG Injector
// ? npm i svg-injector -D
import SVGInjector from "svg-injector";

export function randomId() {
	return uuidv1();
}

export function injectSvg() {
	SVGInjector(document.querySelectorAll("img.inject"));
}

// * Awaiter
export function awaiter(ms) {
	new Promise((resolve) => setTimeout(resolve, ms));
}
