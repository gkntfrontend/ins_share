import Vue from "vue";
import vSelect from "vue-select";

vSelect.props.components.default = () => ({
  // Deselect: {
  //   render: createElement => createElement("i", { class: ["clear-btn"] })
  // },
  OpenIndicator: {
    render: createElement => createElement("i", { class: ["dropdown-btn"] })
  }
});

Vue.component("v-select", vSelect);
