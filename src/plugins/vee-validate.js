import Vue from "vue";
import { ValidationProvider, ValidationObserver } from "vee-validate";
import { extend } from "vee-validate";
import tr from "vee-validate/dist/locale/tr";
import * as rules from "vee-validate/dist/rules";

// ? https://logaretm.github.io/vee-validate/api/rules.html
for (let rule in rules) {
	extend(rule, {
		...rules[rule],
		message: tr.messages[rule]
	});
}

Vue.component("ValidationProvider", ValidationProvider);
Vue.component("ValidationObserver", ValidationObserver);
