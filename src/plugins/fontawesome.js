import Vue from "vue";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import {
	faSearch as farSearch,
	faStar as farStar,
  faChevronLeft,
  faChevronRight,
  faCheck
} from "@fortawesome/pro-regular-svg-icons";

import {
	faInfoCircle,
	faEye,
	faEyeSlash
} from "@fortawesome/pro-solid-svg-icons";

import {
	faTwitter,
	faInstagram,
	faFacebookSquare,
	faVk,
	faWhatsapp,
	faTelegramPlane
} from "@fortawesome/free-brands-svg-icons";

library.add(
  faChevronLeft,
  faChevronRight,
	farSearch,
	faInfoCircle,
	faEye,
	faEyeSlash,
	farStar,
  faCheck,
	faTwitter,
	faInstagram,
	faFacebookSquare,
	faVk,
	faWhatsapp,
	faTelegramPlane
);

Vue.component("fa5-icon", FontAwesomeIcon);
